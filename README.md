# Descripción

Proyecto para administrar la conexion a la cola de rabbitMQ

## Obtener todos los paquetes

go get -d -u gitlab.com/woh-group/backend/mq-rabbit/...

## Crear modulo proyecto

go mod init gitlab.com/woh-group/backend/mq-rabbit
